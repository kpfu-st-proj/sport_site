from extensions import db
from shop.models import User, Role
from werkzeug.security import generate_password_hash
import configparser
from app import create_app


def create_superuser(username, email, password):
    app = create_app()
    with app.app_context():
        config = configparser.ConfigParser()
        config.read('config.ini')
        head = config.get('PATH', 'ROLE_ALL_CONTROL')

        if Role.query.filter_by(role_name=head).first() is None:
            r = Role(role_name=head, description='Все права и полномочия.')
            db.session.add(r)
            db.session.commit()

        if User.query.filter_by(email=email).first() is not None:
            return 'Пользователь с таким email уже существует.'
        u = User(name=username, email=email, password=generate_password_hash(password), roles=head)
        db.session.add(u)
        db.session.commit()
        return 'Суперпользователь создан!'


if __name__ == '__main__':
    name = input('Имя пользователя:')
    email = input('Email:')
    password = input('Пароль:')
    result = create_superuser(name, email, password)
    print(result)

from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_admin import Admin
from admin_models import HomeAdminView


db = SQLAlchemy()
migrate = Migrate()
admin = Admin(index_view=HomeAdminView(name='Home'), url='/', name='На сайт')

from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField, SelectMultipleField
from wtforms.validators import DataRequired, Email, Length
from wtforms.fields.html5 import EmailField
from wtforms.widgets import TextArea, ListWidget, CheckboxInput


class RegistrationForm(FlaskForm):
    username = StringField('Имя:', validators = [
        Length(max=100, message='Имя пользователя должно быть не более 100 символов')
    ])
    email = EmailField('Email:', validators = [
        DataRequired(),
        Email(),
        Length(max=100, message='Email должен быть не более 100 символов.')
    ])
    password = PasswordField('Пароль:', validators = [
        DataRequired(),
        Length(max=255, message='Пароль должен быть не более 255 символов.')
    ])

    submit = SubmitField('Зарегистрироваться')


class LoginForm(FlaskForm):
    login = EmailField('Логин:', validators = [
        DataRequired(),
        Email(),
        Length(max=100, message='Логин должен быть не более 100 символов.')
    ])
    password = PasswordField('Пароль:', validators=[
        DataRequired(),
        Length(max=255, message='Пароль должен быть не более 255 символов.')
    ])

    remember_me = BooleanField('Запомнить меня')

    submit = SubmitField('Войти')


class ReviewForm(FlaskForm):
    review_text = StringField('Отзыв:', widget=TextArea(), validators=[
        Length(max=500, message='Отзыв должен быть не более 500 символов'),
        DataRequired()
    ])

    submit = SubmitField('Отправить')


class ChangePersonalData(FlaskForm):
    password = PasswordField(
        'Пароль', validators=[Length(max=255, message='Пароль должен быть не более 255 символов')]
    )
    login = EmailField(
        'Логин:', validators=[Email(), Length(max=100, message='Логин должен быть не более 100 сиволов.')]
    )
    name = StringField(
        'Имя пользователя', validators=[Length(max=100, message='Имя пользователя должно быть не более 100 символов.')]
    )

    submit = SubmitField('Отправить')


class MultiCheckboxField(SelectMultipleField):
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()


class FilterForm(FlaskForm):
    brand = MultiCheckboxField('Бренд', coerce=int)
    type_product = MultiCheckboxField('Категория', coerce=int)
    price_from = StringField('от:')
    price_to = StringField('до:')
    submit = SubmitField('Применить')

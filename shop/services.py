from werkzeug.security import check_password_hash, generate_password_hash
from extensions import db
from .models import User, Purchase, Product, Subdivision, Review, Brand, ProductType
from flask import url_for


class NotFoundError(Exception):
    pass


class IncorrectPasswordError(Exception):
    pass


class UserAlreadyExists(Exception):
    pass


def find_by_username_and_password(login: str, password: str):
    user = User.query.filter_by(email=login).first()
    if not user:
        raise NotFoundError('Неправильный логин или пароль')
    if not check_password_hash(user.password, password):
        raise IncorrectPasswordError('Неправильный логин или пароль')
    return user


def find_user_by_id(user_id: int):
    user = User.query.filter_by(id=user_id).first()
    if not user:
        raise NotFoundError()
    return user


def registrate_user(username, login, password):
    user = User.query.filter_by(email=login).first()
    if user:
        raise UserAlreadyExists('Пользователь с таким email уже существует')
    hashed_password = generate_password_hash(password)
    user = User(
        name=username,
        email=login,
        password=hashed_password
    )
    db.session.add(user)
    db.session.commit()

    return None


def change_personal_data(user_id, new_password, new_name, new_login):
    user = find_user_by_id(user_id)
    if new_password is not None:
        user.password = generate_password_hash(new_password)
    if new_name is not None:
        user.name = new_name
    if new_login is not None:
        user.email = new_login
    db.session.commit()
    return user


def get_purchase(user_id):
    purchase_records = Purchase.query.filter_by(user=user_id)
    if purchase_records is None:
        return

    completed_records = []
    for record in purchase_records:
        product = Product.query.filter_by(id=record.product)
        complete_record = {
            'id': record.id,
            'date': record.datetime,
            'title': product.price,
            'price': product.price
        }
        completed_records.append(complete_record)

    return completed_records


def get_products_by_subdivision(subdivision):
    products = Product.query.filter_by(subdivision=subdivision).all()
    return products


def get_max_price(products):
    max_pr = 0

    for p in products:
        if p.price > max_pr:
            max_pr = p.price
    return max_pr


def get_subdivisions():
    subdivisions = Subdivision.query.all()
    return subdivisions


def get_product(product_id):
    product = Product.query.filter_by(id=product_id).first()
    return product


def get_current_product_all_info(product_id):
    product = get_product(product_id)
    reviews = Review.query.filter_by(product=product_id).all()
    return product, reviews[::-1]


def create_review(text, user_id, product_id):
    review = Review(text=text, user=user_id, product=product_id)
    db.session.add(review)
    db.session.commit()
    return review


def get_all_products_in_cart(cart_from_session):
    products = {}

    for key, item in cart_from_session.items():
        product = get_product(key)
        products[item] = product

    return products


def get_filters_for_products():
    brands = Brand.query.all()
    types = ProductType.query.all()
    brands_choices = []
    types_choices = []
    for b in brands:
        brands_choices.append((b.id, b.title))

    for t in types:
        types_choices.append((t.id, t.title))

    return brands_choices, types_choices


def filtered_products(brand, product_type, price_from, price_to):
    with_brand_and_type_filters = db.session.query(Product).filter(
        Product.brand.in_(brand),
        Product.product_type.in_(product_type)
    )
    filtered = []

    for prod in with_brand_and_type_filters:
        if int(price_from) <= prod.price <= int(price_to):
            filtered.append(prod)

    result = []
    for prod in filtered:
        result.append({
            'title': prod.title,
            'photo_path': prod.photo_path,
            'price': prod.price,
            'brand': prod.brand,
            'product_type': prod.product_type,
            'description': prod.description,
            'current_product_url': url_for('shop.current_product', product_id=prod.id)
        })

    return result

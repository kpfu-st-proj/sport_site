function removeFromCart(product_id) {
    document.getElementById(product_id).addEventListener('submit', (e) => {
        e.preventDefault();
        fetch(e.target.getAttribute('action'))
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                var elem = document.getElementById(data.product_id);
                elem.parentNode.removeChild(elem)
            })
    });
}

function add_listeners() {
    document.getElementById('review').addEventListener('submit', function(e) {
        e.preventDefault()
    });

    document.getElementById('add-on-cart').addEventListener('submit', function(e) {
        e.preventDefault()
    });
}


add_listeners()


function insertNewReview(data) {
    var ul = document.getElementById("new-review");
    ul.insertAdjacentHTML('afterBegin', '<li class="list-group-item">' + data.text + '</li>');
}


function sendReview(e) {
    const f = document.getElementById('review')   
    const formData = new FormData(f);
    fetch(f.getAttribute('action'), {
        method: f.getAttribute('method'),
        body: formData
    })
    .then((response) => {
        return response.json();
    })
    .then((data) => {
        insertNewReview(data);
        document.getElementById('review').reset();
    });
}


function addOnCart(e) {
    const f = document.getElementById('add-on-cart')
    fetch(f.getAttribute('action'));
}

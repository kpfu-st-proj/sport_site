function add_listener() {
    document.getElementById('filter-form').addEventListener('submit', function(e) {
        e.preventDefault()
    });
}


add_listener()


function insertFilteredProducts (data) {
    thread = document.getElementById('products-thread')
    thread.innerHTML = "";

    for (prod in data) {
        thread.insertAdjacentHTML('afterBegin', 
          `<div class="col">
            <div class="card h-100">
              <a href=` + data[prod]['current_product_url'] + `>
              <img src="` + `../` + data[prod]['photo_path'] + `" class="card-img-top" alt="...">
              <div class="card-body">
                <h6>` + data[prod]['title']+ `</h6>
                <p class="cart-text">` + data[prod]['price'] + ` руб</p>
              </div>
            </a>
            </div>
          </div>`
        )
    }

}


function sendFilters(e) {
    var element = document.querySelector('input[type=checkbox]');
    const f = document.getElementById('filter-form');
    const formData = new FormData(f);
    fetch(f.getAttribute('action'), {
        method: f.getAttribute('method'),
        body: formData
    })
    .then((response) => {
        return response.json()
    })
    .then((data) => {
        insertFilteredProducts(data);
    });
}

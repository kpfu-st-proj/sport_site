from extensions import db
from datetime import datetime
from flask_login import UserMixin


class Role(db.Model):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    role_name = db.Column(db.String(100), unique=True, nullable=False)
    description = db.Column(db.String(500), nullable=False)
    relationship = db.relationship('User', backref='user_roles')

    def __repr__(self):
        return self.role_name


class User(db.Model, UserMixin):
    __tablename__ = 'site_users'
    id = db.Column(db.Integer, db.Identity(), primary_key=True)
    name = db.Column(db.String(100))
    email = db.Column(db.String(100), unique=True, nullable=False)
    password = db.Column(db.String(255), nullable=False)
    roles = db.Column(db.String(100), db.ForeignKey('roles.role_name'))
    post_comment = db.relationship('Comment', backref='comment_creator_email', cascade='all, delete')
    product_review = db.relationship('Review', backref='review_creator_id', cascade='all, delete')
    saved = db.relationship('SavedPosts', backref='saved', cascade='all, delete')

    def __repr__(self):
        return self.email


class Product(db.Model):
    __tablename__ = 'products'
    id = db.Column(db.Integer, db.Identity(), primary_key=True)
    title = db.Column(db.String(240), nullable=False)
    description = db.Column(db.String(1000))
    subdivision = db.Column(db.String(100), db.ForeignKey('subdivisions.title'), nullable=False)
    price = db.Column(db.Integer, nullable=False)
    photo_path = db.Column(db.String(200), nullable=False)
    brand = db.Column(db.Integer, db.ForeignKey('brands.id'), nullable=False)
    product_type = db.Column(db.Integer, db.ForeignKey('types_of_product.id'), nullable=False)
    review = db.relationship('Review', backref='product_id', cascade='all, delete')
    purchase_order = db.relationship('Purchase', cascade='all, delete', backref='product_id')

    def __repr__(self):
        return self.title


class Purchase(db.Model):
    __tablename__ = 'purchase_history'
    id = db.Column(db.Integer, db.Identity(), primary_key=True)
    datetime = db.Column(db.DateTime, default=datetime.now())
    product = db.Column(db.Integer, db.ForeignKey('products.id'))
    user = db.Column(db.Integer, db.ForeignKey('site_users.id'))


class Review(db.Model):
    __tablename__ = 'product_review'
    id = db.Column(db.Integer, db.Identity(), primary_key=True)
    text = db.Column(db.String(500), nullable=False)
    user = db.Column(db.Integer, db.ForeignKey('site_users.id'))
    product = db.Column(db.Integer, db.ForeignKey('products.id'))
    date = db.Column(db.DateTime, default=datetime.now())


class Brand(db.Model):
    __tablename__ = 'brands'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), unique=True, nullable=False)
    product = db.relationship('Product', backref='brand_title', cascade='all, delete')

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title


class ProductType(db.Model):
    __tablename__ = 'types_of_product'
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(200), unique=True, nullable=False)
    product = db.relationship('Product', backref='type_product', cascade='all, delete')

    def __str__(self):
        return self.title

    def __repr__(self):
        return self.title


class Subdivision(db.Model):
    __tablename__ = 'subdivisions'
    title = db.Column(db.String(100), primary_key=True, nullable=False)
    photo_path = db.Column(db.String(100), nullable=False)
    product = db.relationship('Product', backref='product_subdivision')

    def __repr__(self):
        return self.title

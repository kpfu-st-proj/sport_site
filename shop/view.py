from flask import Blueprint, render_template, redirect, request, flash, url_for, session
from flask_login import login_user, login_required, logout_user, current_user

from .import services
from .forms import RegistrationForm, LoginForm, ReviewForm, ChangePersonalData, FilterForm

import json

shop_blueprint = Blueprint('shop', __name__, template_folder='templates', static_folder='shop_static')


@shop_blueprint.route('/', methods=('POST', 'GET'))
def main():
    subdivisions = services.get_subdivisions()
    return render_template('shop/main.html', subdivisions=subdivisions)


@shop_blueprint.route('/login', methods=('GET', 'POST'))
def login():
    if current_user.is_authenticated:
        return redirect(url_for('shop.personal_area', user_id=current_user.id))
    form = LoginForm()
    if request.method == 'POST':
        login = form.login.data
        password = form.password.data
        remember_me = form.remember_me.data
        try:
            user = services.find_by_username_and_password(login, password)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('shop.login'))
        else:
            login_user(user, remember=remember_me)
            return redirect(url_for('shop.personal_area', user_id=user.id))

    return render_template('shop/login.html', form=form)


@shop_blueprint.route("/logout", methods=["GET"])
@login_required
def logout():
    logout_user()
    return redirect(url_for('shop.main'))


@shop_blueprint.route('/registrate', methods=('POST', 'GET'))
def registrate():
    form = RegistrationForm()
    if current_user.is_authenticated:
        return redirect(url_for('shop.personal_area'))
    if form.validate_on_submit():
        password = form.password.data
        login = form.email.data
        username = form.username.data
        try:
            services.registrate_user(username, login, password)
        except Exception as e:
            flash(str(e) or 'Unknown error')
            return redirect(url_for('shop.registrate'))
        else:
            flash('Вы успешно зарегистрированы!')
            return redirect(url_for('shop.login'))

    return render_template('shop/registrate.html', form=form)


@shop_blueprint.route('/personal_area/<int:user_id>')
@login_required
def personal_area(user_id):
    purchase_history = services.get_purchase(user_id)
    admin_access = False
    if current_user.roles is not None:
        admin_access = True
    return render_template('shop/personal_area.html', purchase_history=purchase_history, admin_access=admin_access)


@shop_blueprint.route('/personal_area/<int:user_id>/change_personal_data', methods=['GET', 'POST'])
@login_required
def change_personal_data(user_id):
    form = ChangePersonalData()
    if request.method == 'POST':
        services.change_personal_data(user_id, form.password.data, form.name.data, form.login.data)
        return redirect(url_for('shop.personal_area', user_id=user_id))
    return render_template('shop/change_personal_data.html', form=form)


@shop_blueprint.route('/shop_thread/<string:subdivision>', methods=['POST', 'GET'])
def shop_thread(subdivision):
    form = FilterForm()
    form.brand.choices, form.type_product.choices = services.get_filters_for_products()
    products = services.get_products_by_subdivision(subdivision)
    max_price = services.get_max_price(products)

    if form.validate_on_submit():
        brand_choice = tuple(form.brand.data)
        type_product_choice = tuple(form.type_product.data)
        price_from = form.price_from.data
        price_to = form.price_to.data
        filtered = services.filtered_products(brand_choice, type_product_choice, price_from, price_to)
        return json.dumps(filtered)

    return render_template(
        'shop/shop_thread.html', products=products, form=form, subdivision=subdivision, max_price=max_price
    )


@shop_blueprint.route('shop/product/<string:product_id>')
def current_product(product_id):
    form = ReviewForm()
    product, reviews = services.get_current_product_all_info(product_id)
    return render_template('shop/product.html', form=form, product=product, reviews=reviews)


@shop_blueprint.route('shop/product/<string:product_id>/add_reveiw', methods=['POST'])
@login_required
def review_handler(product_id):
    form = ReviewForm()
    json_data = {}
    if request.method == 'POST':
        text = form.review_text.data
        review = services.create_review(text, current_user.id, int(product_id))
        json_data['text'] = text

        return json_data


@shop_blueprint.route('product_cart/<string:product_id>/add_on_cart', methods=['GET', 'POST'])
def product_cart(product_id):
    product = services.get_product(product_id)

    if 'cart' in session:
        if not product_id in list(session['cart'].keys()):
            session['cart'][product_id] = 1
            session.modified = True
        else:
            session['cart'][product_id] += 1
            session.modified = True
    else:
        session['cart'] = {product_id: 1}

    return {'status':200}


@shop_blueprint.route('/cart')
def cart():
    cart_for_user = None
    if 'cart' in session:
        cart_in_session = session['cart']
        cart_for_user = services.get_all_products_in_cart(cart_in_session)
    return render_template('shop/cart.html', cart=cart_for_user)


@shop_blueprint.route('/cart/<string:product_id>')
def remove_from_cart(product_id):
    session['cart'].pop(product_id)
    session.modified = True
    return {'product_id': product_id}

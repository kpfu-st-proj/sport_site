from flask_admin.contrib.sqla import ModelView
from flask_admin import form, AdminIndexView
from flask import redirect, url_for, request
from flask_login import current_user
from werkzeug.security import generate_password_hash
import random
import os
import configparser


class AdminConfig:
    global config
    config = configparser.ConfigParser()
    config.read('config.ini')


class AdminMixin:
    def inaccessible_callback(name, **kwargs):
        return redirect(url_for('shop.login', next=request.url))


class ShopAccessView(AdminMixin, ModelView):
    def is_accessible(self):
        return current_user.roles in config.get('PATH', 'ROLE_HAVE_ACCESS_TO_SHOP')


class PostAccessView(AdminMixin, ModelView):
    def is_accessible(self):
        return current_user.roles in config.get('PATH', 'ROLE_HAVE_ACCESS_TO_NEWS')


class AllAccessView(AdminMixin, ModelView):
    def is_accessible(self):
        return current_user.roles == config.get('PATH', 'ROLE_ALL_CONTROL')


class HomeAdminView(AdminMixin, AdminIndexView):
    def is_accessible(self):
        try:
            access = current_user.roles in config.get('PATH', 'ROLE_HAVE_ACCESS_TO_SHOP') or config.get('PATH', 'ROLE_HAVE_ACCESS_TO_NEWS')
        except AttributeError:
            return False
        except TypeError:
            return False
        else:
            if access is None:
                return False
            else:
                return access


class ProductAdminView(ShopAccessView, ModelView):
    PRODUCT_IMAGES_PATH = config.get('PATH', 'PRODUCT_IMAGES_PATH')

    column_searchable_list = ['id', 'title']

    column_display_pk = True

    form_extra_fields = {
        'file': form.FileUploadField('Фото продукта', base_path=PRODUCT_IMAGES_PATH)
    }

    form_columns = (
        'title',
        'description',
        'price',
        'product_subdivision',
        'brand_title',
        'type_product',
        'file',
        'photo_path'
    )

    column_labels = dict(
        product_subdivision='Подразделение',
        description='Описание',
        price='Цена',
        title='Название',
        brand_title='Название бренда',
        type_product='Тип продукта',
        photo_path='Фото'
    )

    form_widget_args = {'photo_path': {'disabled':True}}

    def _change_path_data(self, _form):
        PRODUCT_IMAGES_PATH = config.get('PATH', 'PRODUCT_IMAGES_PATH')
        try:
            if not os.path.exists(PRODUCT_IMAGES_PATH):
                os.mkdir(PRODUCT_IMAGES_PATH)

            storage_file = _form.file.data

            if storage_file is not None:
                hash = random.getrandbits(128)
                ext = storage_file.filename.split('.')[-1]
                photo_path = '%s.%s' % (hash, ext)
                storage_file.save(
                    os.path.join(PRODUCT_IMAGES_PATH, photo_path)
                )
                RELATIVE_PATH = '/'.join(PRODUCT_IMAGES_PATH.split('/')[-3:])
                _form.photo_path.data = RELATIVE_PATH + photo_path

                del _form.file

        except Exception as ex:
            pass

        return _form

    def edit_form(self, obj=None):
        return self._change_path_data(
            super(ProductAdminView, self).edit_form(obj)
        )

    def create_form(self, obj=None):
        return self._change_path_data(
            super(ProductAdminView, self).create_form(obj)
        )


class UserAdminView(AllAccessView, ModelView):
    column_searchable_list = ['id', 'name']
    column_labels = dict(
        name='Имя',
        password='Пароль',
        roles='Роль',
        user_roles='Роль'
    )

    column_display_pk = True

    form_columns = ('name', 'password', 'email', 'user_roles')

    def on_model_change(self, form, User, is_created=False):
        password_hash = generate_password_hash(form.password.data)
        User.password = password_hash


class RoleAdminView(AllAccessView, ModelView):
    column_labels = dict(
        role_name='Название роли',
        description='Полномочия/Описание'
    )

    form_columns = ('role_name', 'description')


class BrandAdminView(ShopAccessView, ModelView):
    column_labels = dict(
        title='Название бренда'
    )

    form_columns = ('title',)


class ProductTypeAdminView(ShopAccessView, ModelView):
    column_labels = dict(
        title='Категория товара'
    )

    form_columns = ('title',)


class SubdivisionAdminView(ShopAccessView, ModelView):
    config = configparser.ConfigParser()
    config.read('config.ini')

    SUBDIVISION_IMAGES_PATH = config.get('PATH', 'SUBDIVISION_IMAGES_PATH')

    form_widget_args = {'photo_path': {'disabled': True}}

    column_labels = dict(
        title='Название подразделения',
        photo_path='Фото',
        file='Фото'
    )

    form_columns = ('title', 'file', 'photo_path')

    column_list = ('title',)

    form_extra_fields = {
        'file': form.FileUploadField('Фото', base_path=SUBDIVISION_IMAGES_PATH)
    }

    def _change_path_data(self, _form):
        SUBDIVISION_IMAGES_PATH = config.get('PATH', 'SUBDIVISION_IMAGES_PATH')
        try:
            if not os.path.exists(SUBDIVISION_IMAGES_PATH):
                os.mkdir(SUBDIVISION_IMAGES_PATH)

            storage_file = _form.file.data
            if storage_file is not None:
                hash = random.getrandbits(128)
                ext = storage_file.filename.split('.')[-1]
                photo_path = '%s.%s' % (hash, ext)
                storage_file.save(
                    os.path.join(SUBDIVISION_IMAGES_PATH, photo_path)
                )
                RELATIVE_PATH = '/'.join(SUBDIVISION_IMAGES_PATH.split('/')[-3:])

                _form.photo_path.data = RELATIVE_PATH + photo_path

                del _form.file

        except Exception as ex:
            pass

        return _form

    def edit_form(self, obj=None):
        return self._change_path_data(
            super(SubdivisionAdminView, self).edit_form(obj)
        )

    def create_form(self, obj=None):
        return self._change_path_data(
            super(SubdivisionAdminView, self).edit_form(obj)
        )


class TagAdminView(PostAccessView, ModelView):
    column_labels = dict(
        title='Название тега',
    )

    form_columns = ('title',)


class PostAdminView(PostAccessView, ModelView):
    config = configparser.ConfigParser()
    config.read('config.ini')

    POST_IMAGES_PATH = config.get('PATH', 'POST_IMAGES_PATH')

    column_searchable_list = ['id', 'title']

    form_widget_args = {'photo_path': {'disabled': True}}

    column_labels = dict(
        tag='Теги',
        title='Название поста',
        text='Текст',
        created='Создан',
        photo_path='Изображение',
        post_likes_id='Количество лайков',
        file='Изображение'
    )

    column_list = ('id', 'title', 'text', 'photo_path', 'tag')

    form_columns = ('title', 'text', 'file', 'photo_path', 'tag')

    form_extra_fields = {
        'file': form.FileUploadField('Фото поста', base_path=POST_IMAGES_PATH)
    }

    def _change_path_data(self, _form):
        POST_IMAGES_PATH = config.get('PATH', 'POST_IMAGES_PATH')
        try:
            if not os.path.exists(POST_IMAGES_PATH):
                os.mkdir(POST_IMAGES_PATH)

            storage_file = _form.file.data
            if storage_file is not None:
                hash = random.getrandbits(128)
                ext = storage_file.filename.split('.')[-1]
                photo_path = '%s.%s' % (hash, ext)
                storage_file.save(
                    os.path.join(POST_IMAGES_PATH, photo_path)
                )

                RELATIVE_PATH = '/'.join(POST_IMAGES_PATH.split('/')[-3:])
                _form.photo_path.data = RELATIVE_PATH + photo_path

                del _form.file

        except Exception as ex:
            pass

        return _form

    def edit_form(self, obj=None):
        return self._change_path_data(
            super(PostAdminView, self).edit_form(obj)
        )

    def create_form(self, obj=None):
        return self._change_path_data(
            super(PostAdminView, self).create_form(obj)
        )

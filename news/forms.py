from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import Length
from wtforms.widgets import TextArea


class CommentForm(FlaskForm):
    text = StringField(
        "Комментарий",
        widget=TextArea(),
        validators=[Length(max=500, message='Комментарий должен быть не более 500 символов.')])

    submit = SubmitField('Отправить')

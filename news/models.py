from extensions import db
from datetime import datetime
import re


class Tag(db.Model):
    __tablename__ = 'tags'
    title = db.Column(db.String(100), primary_key=True)

    def __repr__(self):
        return self.title


post_tag = db.Table(
    'post_tag',
    db.Column('tag_title', db.String(100), db.ForeignKey('tags.title')),
    db.Column('post_id', db.Integer, db.ForeignKey('posts.id'))
)


class Post(db.Model):
    __tablename__ = 'posts'
    id = db.Column(db.Integer, db.Identity(), primary_key=True)
    tag = db.relationship('Tag', secondary=post_tag, backref=db.backref('posts', lazy='dynamic'))
    title = db.Column(db.String(140), nullable=False)
    text = db.Column(db.String(3000))
    created = db.Column(db.DateTime, default=re.findall(r'.*\s\d\d:\d\d', str(datetime.now()))[0])
    photo_path = db.Column(db.String(200), nullable=False)
    comment = db.relationship('Comment', backref='post_c_id', cascade='all, delete')
    saved_post = db.relationship('SavedPosts', backref='saved_post', cascade='all, delete')

    def __repr__(self):
        return self.title


class SavedPosts(db.Model):
    __tablename__ = 'saved_posts'
    id = db.Column(db.Integer, db.Identity(), primary_key=True)
    user_id = db.Column(db.Integer, db.ForeignKey('site_users.id'))
    post_id = db.Column(db.Integer, db.ForeignKey('posts.id'))


class Comment(db.Model):
    __tablename__ = 'post_comments'
    id = db.Column(db.Integer, db.Identity(), primary_key=True)
    user = db.Column(db.Integer, db.ForeignKey('site_users.id'))
    post = db.Column(db.Integer, db.ForeignKey('posts.id'))
    text = db.Column(db.String(500))

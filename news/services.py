from .models import Post, Comment, SavedPosts
from extensions import db


def get_posts():
    posts = Post.query.all()
    return posts


def get_post_by_id(post_id):
    post = Post.query.filter_by(id=post_id).first()
    return post


def get_post_all_info(post_id):
    post = get_post_by_id(post_id)
    comments = Comment.query.filter_by(post=post_id).all()
    return post, comments[::-1]


def add_comment(user_id, post_id, text):
    comment = Comment(user=user_id, post=post_id, text=text)
    db.session.add(comment)
    db.session.commit()
    return comment


def add_on_liked(user_id, post_id):
    p = SavedPosts.query.filter_by(user_id=user_id, post_id=post_id).first()
    if p:
        return False

    p = SavedPosts(user_id=user_id, post_id=post_id)
    db.session.add(p)
    db.session.commit()

    return True


def get_all_liked(user_id):
    records = SavedPosts.query.filter_by(user_id=user_id).all()
    posts = []
    for r in records:
        p = Post.query.filter_by(id=r.post_id).first()
        posts.append(p)
    return posts


def remove_from_liked(post_id, user_id):
    record = SavedPosts.query.filter_by(post_id=post_id, user_id=user_id).first()
    db.session.delete(record)
    db.session.commit()
    return record.post_id

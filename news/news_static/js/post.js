function add_listeners() {
    document.getElementById('comment').addEventListener('submit', function(e) {
        e.preventDefault()
    });

    document.getElementById('add-on-liked').addEventListener('submit', function(e) {
        e.preventDefault()
    });
}


add_listeners()


function insertNewComment(data) {
    var ul = document.getElementById("new-comment");
    ul.insertAdjacentHTML('afterBegin', '<li class="list-group-item">' + data.text + '</li>');
}


function sendComment(e) {
        const f = document.getElementById('comment')
        const formData = new FormData(f);
        fetch(f.getAttribute('action'), {
            method: f.getAttribute('method'),
            body: formData
        })
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            insertNewComment(data);
            document.getElementById('comment').reset();
        });
}


function insertAlert(text) {
    var f = document.getElementById("add-on-liked");
    f.insertAdjacentHTML('beforeEnd', '<div class="alert alert-light" role="alert">' + text + '</div>');
}


function addOnLiked(e) {
    const f = document.getElementById('add-on-liked')
    fetch(f.getAttribute('action'))
        .then((response) => {
            return response.json();
        })
        .then((data) => {
            if (data.resp == true) {
                insertAlert('Добавлено')
            }
        });
}
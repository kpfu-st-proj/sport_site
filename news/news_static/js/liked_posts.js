function removeFromLiked(post_id) {
    document.getElementById(post_id).addEventListener('submit', (e) => {
        e.preventDefault();
        fetch(e.target.getAttribute('action'))
            .then((response) => {
                return response.json();
            })
            .then((data) => {
                var elem = document.getElementById('post'+data.post_id);
                elem.parentNode.removeChild(elem);
            });
    });
}

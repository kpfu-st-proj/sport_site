from flask import Blueprint, render_template
from .import services
from .forms import CommentForm
from flask_login import current_user, login_required


news = Blueprint('news', __name__, template_folder='templates', static_folder='news_static')


@news.route('/')
def main():
    posts = services.get_posts()
    return render_template('news/main.html', posts = posts)


@news.route('/<string:post_id>')
def post(post_id):
    form = CommentForm()
    post, comments = services.get_post_all_info(post_id)
    return render_template('news/post.html', post=post, comments = comments, form=form)


@news.route('/<string:post_id>/add_comment', methods=['POST'])
@login_required
def add_comment(post_id):
    form = CommentForm()
    json_data = {}
    text = form.text.data
    comment = services.add_comment(current_user.id, post_id, text)
    json_data['text'] = text

    return json_data


@news.route('/<string:post_id>/add_on_liked', methods=['POST', 'GET'])
@login_required
def add_on_liked(post_id):
    is_added = services.add_on_liked(current_user.id, post_id)
    return {'resp': is_added}


@news.route('/liked_posts')
@login_required
def liked_posts():
    posts = services.get_all_liked(current_user.id)
    return render_template('/news/liked_posts.html', posts=posts)


@news.route('/liked_posts/remove/<string:post_id>', methods=['POST', 'GET'])
@login_required
def remove_from_liked(post_id):
    removed = services.remove_from_liked(post_id, current_user.id)
    return {'post_id': removed}

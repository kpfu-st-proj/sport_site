Create a .env file and set your following variables:
SQLALCHEMY_DATABASE_URI
SECRET_KEY

To create a superuser (User with all rights on the site), you need to enter: "python createsuperuser.py"

Apply migrations: flask upgrade
Running the project: "flask run" or "python app.py" 

To go to the admin panel, you will need to log in and go to the personal account tab, there will be a "Admin" button. (After you have created the superuser, of course.)

Python 3.9.7

from flask import Flask
from flask_wtf.csrf import CSRFProtect
import admin_models

from dotenv import load_dotenv

from shop import models as shop_models
from news import models as news_models
from extensions import db, migrate, admin
from shop.view import shop_blueprint
from news.view import news
from shop.login_manager import login_manager


def create_app():
    load_dotenv()
    app = Flask(__name__.split('.')[0])
    app.config.from_object('config.Configuration')
    register_extensions(app)
    register_blueprints(app)
    admin_area()
    return app


def register_extensions(app):
    CSRFProtect(app)
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    admin.init_app(app)
    return None


def admin_area():
    admin.add_view(admin_models.UserAdminView(shop_models.User, db.session, u'Пользователи'))
    admin.add_view(admin_models.ProductAdminView(shop_models.Product, db.session, u'Продукты'))
    admin.add_view(admin_models.RoleAdminView(shop_models.Role, db.session, u'Роли'))
    admin.add_view(admin_models.BrandAdminView(shop_models.Brand, db.session, u'Бренды'))
    admin.add_view(admin_models.ProductTypeAdminView(shop_models.ProductType, db.session, u'Категории продуктов'))
    admin.add_view(admin_models.SubdivisionAdminView(shop_models.Subdivision, db.session, u'Подразделения'))
    admin.add_view(admin_models.TagAdminView(news_models.Tag, db.session, u'Теги'))
    admin.add_view(admin_models.PostAdminView(news_models.Post, db.session, u'Посты'))
    return None


def register_blueprints(app):
    app.register_blueprint(shop_blueprint, url_prefix='/')
    app.register_blueprint(news, url_prefix='/news')
    return None


def main():
    app = create_app()
    app.run()
    return app


if __name__ == '__main__':
    main()
